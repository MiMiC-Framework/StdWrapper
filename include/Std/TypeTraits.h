#ifndef STD_TYPETRAITS_H
#define STD_TYPETRAITS_H

#include <type_traits>

using std::remove_cv_t;
using std::remove_reference_t;

template <class... B>
using Conjunction = std::conjunction<B...>;

template <class... B>
inline constexpr bool ConjunctionV = std::conjunction_v<B...>;

template <bool B, class T, class F>
using ConditionalT = std::conditional_t<B,T,F>;

template <bool B, class A = void>
using EnableIf = std::enable_if<B, A>;

template <bool B, class A = void>
using EnableIfT = std::enable_if_t<B, A>;

template <typename Fn_, typename... Args>
using IRes = std::invoke_result_t<Fn_, Args...>;

template <class A>
using IsCopyAssignable = std::is_copy_assignable<A>;

template <class A>
inline constexpr bool IsCopyAssignableV =
    std::is_copy_assignable_v<A>;

template <class A>
using IsCopyConstructible = std::is_copy_constructible<A>;

template <class A>
inline constexpr bool IsCopyConstructibleV =
    std::is_copy_constructible_v<A>;

template <class T, class... Args>
using IsConstructible = std::is_constructible<T, Args...>;

template <class T, class... Args>
inline constexpr bool IsConstructibleV =
    std::is_constructible_v<T, Args...>;

template <typename Fn, typename... Args>
inline constexpr bool IsInv = std::is_invocable<Fn, Args...>::value;

template <class T>
using IsMoveConstructible = std::is_move_constructible<T>;

template <class T>
inline constexpr bool IsMoveConstructibleV =
    std::is_move_constructible_v<T>;

template <class T>
using IsMoveAssignable = std::is_move_assignable<T>;

template <class T>
inline constexpr bool IsMoveAssignableV =
    std::is_move_assignable_v<T>;

template <class A, class... B>
inline constexpr bool IsNoThrowConstructibleV =
    std::is_nothrow_constructible_v<A, B...>;

template <class A>
using IsNoThrowCopyConstructible = std::is_nothrow_copy_constructible<A>;

template <class A>
inline constexpr bool IsNoThrowCopyConstructibleV =
    std::is_nothrow_copy_constructible_v<A>;

template <class A>
using IsNoThrowMoveConstructible =
    std::is_nothrow_move_constructible<A>;

template <class A, class B>
inline constexpr bool IsNoThrowMoveConstructibleV =
    std::is_nothrow_assignable_v<A, B>;

template <class A, class B>
inline constexpr bool IsNoThrowAssignableV =
    std::is_nothrow_assignable_v<A, B>;

template <typename A, typename B>
using IsSame = std::is_same<A,B>;

template <class A, class B>
inline constexpr bool IsSameV = std::is_same_v<A,B>;

template <class T>
struct RmCVRef {
    typedef remove_cv_t<remove_reference_t<T>> type;
};

template <class T>
using RmCVRefT = typename RmCVRef<T>::type;

#endif // TYPETRAITS_H
