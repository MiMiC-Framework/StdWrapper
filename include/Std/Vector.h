#ifndef MS_VECTOR_H
#define MS_VECTOR_H

#include <vector>

template <typename T>
using Vector = std::vector<T>;

#endif
