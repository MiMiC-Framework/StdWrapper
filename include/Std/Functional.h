#ifndef MS_FUNCTIONAL_H
#define MS_FUNCTIONAL_H

#include "Std/Utility.h"

#include <functional>

template <typename A>
using Ref = std::reference_wrapper<A>;

template <typename... Args>
decltype(auto) CRef(Args&&... args) {
    return std::ref(std::forward<Args...>(args...));
}

#endif
