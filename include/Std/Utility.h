#ifndef MS_UTILITY_H
#define MS_UTILITY_H

#include <utility>

using InPlace = std::in_place_t;

template <class T>
using InPlaceType = std::in_place_type_t<T>;

template <std::size_t N>
using InPlaceIndex = std::in_place_index_t<N>;

template <typename A, typename B>
using Pair = std::pair<A,B>;

#endif
