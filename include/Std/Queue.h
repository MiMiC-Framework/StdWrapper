#ifndef MS_QUEUE_H
#define MS_QUEUE_H

#include <queue>

template <typename T>
using Queue = std::deque<T>;

#endif
