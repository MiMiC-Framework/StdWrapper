#ifndef _STDWRAPPER_MUTEX_H
#define _STDWRAPPER_MUTEX_H

#include <mutex>

using Mutex = std::mutex;

#endif
