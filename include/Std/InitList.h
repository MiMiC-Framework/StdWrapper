#ifndef INITLIST_H
#define INITLIST_H

#include <initializer_list>

template <typename T>
using InitList = std::initializer_list<T>;

#endif // INITLIST_H
