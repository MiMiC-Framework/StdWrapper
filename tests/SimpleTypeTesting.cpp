#include <gtest/gtest.h>

#include "../include/Std/Queue.h"
#include "../include/Std/Vector.h"

TEST(Queue, SimpleUsage) {
    Queue<int> q {0};
    q.push_back(1);

    ASSERT_EQ(q.back(), 1);
}

TEST(Vector, SimpleUsage) {
    Vector<int> v1 {0};
    v1.push_back(1);
    Vector<int> v2 {0,1};
    ASSERT_EQ(v1, v2);
}
