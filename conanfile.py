from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class StdWrapperConan(ConanFile):
    name = "stdwrapper"
    version = "0.1"
    license = "GNU GPL v3.0"
    url = "https://gitlab.com/MiMiC-Framework/StdWrapper"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    no_copy_source = True
    generators = "CMakeDeps"

    default_options = {
        "shared": False,
        "tests": True,
        "gtest/*:shared": False,
        "gtest/*:no_gmock": True,
        "gtest/*:no_main": True
    }

    exports_sources = "CMakeLists.txt", "include/*", "tests/*"

    def requirements(self):
        self.test_requires("gtest/1.11.0")

    def layout(self):
        cmake_layout(self)

    def source(self):
        self.run("git clone https://gitlab.com/MiMiC-Framework/StdWrapper")
        self.run("cd StdWrapper && git checkout develop")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_file_name", "StdWrapper")

        self.cpp_info.names["cmake_find_package"] = "StdWrapper"
        self.cpp_info.names["cmake_find_package_multi"] = "StdWrapper"

        # For header-only packages, libdirs and bindirs are not used
        # so it's recommended to set those as empty.
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []

    def package_id(self):
        self.info.clear()
